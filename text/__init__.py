""" from https://github.com/keithito/tacotron """
import re

from text import cleaners
from .symbols import en_symbols, ko_symbols, zh_symbols


# Mappings from symbol to numeric ID and vice versa:
def _symbol_to_id(language):
    assert language in ["en", "ko", "zh"]
    if language == "en":
        symbols = en_symbols
    elif language == "ko":
        symbols = ko_symbols
    elif language == "zh":
        symbols = zh_symbols
    return {s: i for i, s in enumerate(symbols)}

def _id_to_symbol(language):
    assert language in ["en", "ko"]
    if language == "en":
        symbols = en_symbols
    elif language == "ko":
        symbols = ko_symbols
    elif language == "zh":
        symbols = zh_symbols
    return {i: s for i, s in enumerate(symbols)}

# Regular expression matching text enclosed in curly braces:
_curly_re = re.compile(r"(.*?)\{(.+?)\}(.*)")
_in_brackets_re = re.compile(r"\{(.+?)\}")

def text_to_sequence(text, cleaner_names, language):
    '''Converts a string of text to a sequence of IDs corresponding to the symbols in the text.
      The text can optionally have ARPAbet sequences enclosed in curly braces embedded
      in it. For example, "Turn left on {HH AW1 S S T AH0 N} Street."
      Args:
        text: string to convert to a sequence
        cleaner_names: names of the cleaner functions to run the text through
      Returns:
        List of integers corresponding to the symbols in the text
    '''
    sequence = []
    symbol_to_id = _symbol_to_id(language)
    while len(text):
        m = _curly_re.match(text) if language=="en" else None
        if not m:
            m = _in_brackets_re.match(text)
            sequence += _symbols_to_sequence(_clean_text(m.group(1), cleaner_names), symbol_to_id)
            break
        sequence += _symbols_to_sequence(_clean_text(m.group(1), cleaner_names), symbol_to_id)
        sequence += _symbols_to_sequence(m.group(2), symbol_to_id)
        text = m.group(3)
    return sequence

def sequence_to_text(sequence, language):
    '''Converts a sequence of IDs back to a string'''
    result = ''
    id_to_symbol = _id_to_symbol(language)
    for symbol_id in sequence:
        if symbol_id in id_to_symbol:
            s = id_to_symbol[symbol_id]
            # Enclose ARPAbet back in curly braces:
            if len(s) > 1 and s[0] == '@':
                s = '{%s}' % s[1:]
            result += s
    return result.replace('}{', ' ')

def _clean_text(text, cleaner_names=None):
    if cleaner_names == None:
        return text
    for name in cleaner_names:
        cleaner = getattr(cleaners, name)
        if not cleaner:
            raise Exception('Unknown cleaner: %s' % name)
        text = cleaner(text)
    return text

def _symbols_to_sequence(symbols, symbol_to_id):
    # Spaces are removed.
    return [symbol_to_id[s] for s in symbols.split("/") if _should_keep_symbol(s, symbol_to_id)]

def _arpabet_to_sequence(text):
    return _symbols_to_sequence(['@' + s for s in text.split()], _symbol_to_id("en"))

def _should_keep_symbol(s, symbol_to_id):
    return s in symbol_to_id


if __name__ == "__main__":
    print()