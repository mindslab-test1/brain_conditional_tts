from text import cmudict, pinyin


_pad = "_"
_punctuation = "!'(),-.:;?~ "
_silences = ["sp", "spn", "sil"]

# Prepend "@" to ARPAbet symbols to ensure uniqueness (some are the same as uppercase letters):
_arpabet = ["@" + s for s in cmudict.valid_symbols]
_eng_characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
_a_silences = ["@" + s for s in _silences]

_pinyin = pinyin.valid_symbols
_cht_characters = "abcdefghijklmnopqrstuvwxyz12345"

_jamo_leads = "".join([chr(_) for _ in range(0x1100, 0x1113)])
_jamo_vowels = "".join([chr(_) for _ in range(0x1161, 0x1176)])
_jamo_tails = "".join([chr(_) for _ in range(0x11A8, 0x11C3)])
_kor_characters = _jamo_leads + _jamo_vowels + _jamo_tails

# Export all symbols:
en_symbols = list(_pad + _punctuation + _eng_characters) + _arpabet + _a_silences
zh_symbols = list(_pad + _punctuation + _cht_characters) + _pinyin + _silences
ko_symbols = list(_pad + _punctuation + _kor_characters) + _silences