import os
import argparse
import json
import tqdm
import torch
from torch.utils.data import DataLoader
from omegaconf import OmegaConf

from dataset import Dataset
from model.fastspeech2 import FastSpeech2
from preprocessor.preprocessor_gta import Preprocessor
from utils.tools import to_device


META_DIR = "gta_mels"
# In FastSpeech 2, we use alignment from MFA. 
# Instead of filtering alignment from Tacotron 2, use alignment from MFA.

class GtaExtractor(object):
    def __init__(self, args, config):
        self.args = args
        self.config = config
        self._load_checkpoint(args.checkpoint_path)
        self.trainloader = self._gen_train_dataloader()
        self.valloader = self._gen_val_dataloader()
        with open(
            os.path.join(
                config.path.preprocessed_path, 
                "speakers.json",
            )
        ) as f:
            self.speaker_map = {v:k for k,v in json.load(f).items()}
            
    def _load_checkpoint(self, checkpoint_path):
        self.model = FastSpeech2(
            self.config, 
            use_vocoder=False, 
            warm_start=False,
        ).cuda()
        state = torch.load(checkpoint_path, map_location=lambda storage, loc: storage)
        pretrained_dict = state["state_dict"]
        new_model_dict = self.model.state_dict()
        print("Load the following modules from the checkpoint:")
        
        for k, v in pretrained_dict.items():
            if k in new_model_dict and new_model_dict[k].size() == v.size():
                print(k)
                new_model_dict[k] = v

        self.model.load_state_dict(new_model_dict)
        self.model.eval()
        self.model.freeze()
        del state
        torch.cuda.empty_cache()

    def _gen_train_dataloader(self):
        dataset = Dataset(
            "train.txt", 
            self.config, 
            train=False,
            no_cut=True,
        )
        train_dataloader = DataLoader(
            dataset, 
            batch_size=self.config.optimizer.train_batch_size, 
            shuffle=False,
            collate_fn=dataset.collate_fn, 
            num_workers=self.config.optimizer.num_worker,
        )
        return train_dataloader

    def _gen_val_dataloader(self):
        dataset = Dataset(
            "val.txt", 
            self.config, 
            train=False,
            no_cut=True,
        )
        val_dataloader = DataLoader(
            dataset, 
            batch_size=self.config.optimizer.val_batch_size, 
            shuffle=False,
            collate_fn=dataset.collate_fn, 
            num_workers=self.config.optimizer.num_worker,
        )
        return val_dataloader

    def main(self):
        self.extract_and_write_meta("train")
        self.extract_and_write_meta("val")

    def extract_and_write_meta(self, mode):
        assert mode in ["train", "val"]

        dataloader = self.trainloader if mode == "train" else self.valloader
        desc = "Extracting GTA mel of %s data" % mode
        meta_list = list()
        for batch in tqdm.tqdm(dataloader, desc=desc):
            temp_meta = self.extract_gta_mels(batch, mode)
            meta_list.extend(temp_meta)

        root_dir = self.config.path.raw_path
        meta_dir = self.config.path.preprocessed_path
        meta_path = os.path.join(meta_dir, "train.txt") if mode == "train" else os.path.join(meta_dir, "val.txt")
        meta_filename = os.path.basename(meta_path)
        new_meta_filename = "gta_" + meta_filename
        new_meta_path = os.path.join(root_dir, META_DIR, new_meta_filename)

        os.makedirs(os.path.join(root_dir, META_DIR), exist_ok=True)
        with open(new_meta_path, "w", encoding="utf-8") as f:
            for wavpath, speaker in meta_list:
                f.write("%s||%s\n" % (wavpath, speaker))

        print("Wrote %d of %d files to %s" % \
            (len(meta_list), len(dataloader.dataset), new_meta_path))

    @torch.no_grad()
    def extract_gta_mels(self, batch, mode):
        batch = to_device(batch, self.model.device)
        (
            _,
            _,
            savepaths,
            speakers,
            _,
            text_lens,
            _,
            _,
            mel_lens,
            _,
            _,
            _,
            _,
        ) = batch

        output = self.model.forward(*(batch[3:]))
        mel_postnet = output[1]
        return self.store_mels_in_savepaths(
            mel_postnet, savepaths, speakers, text_lens, mel_lens
        )

    def store_mels_in_savepaths(
        self, mel_postnet, savepaths, speakers, input_lengths, output_lengths
    ):
        mels = mel_postnet.detach().cpu()
        input_lengths = input_lengths.cpu()
        output_lengths = output_lengths.cpu()
        speakers = speakers.cpu().tolist()

        temp_meta = list()
        for i, path in enumerate(savepaths):
            t_enc = input_lengths[i]
            t_dec = output_lengths[i]
            speaker_id = speakers[i]
            
            speaker = self.speaker_map[speaker_id]
            
            mel = mels[i].T[:, :t_dec].clone()
            torch.save(mel, path)
            if mel.size(1) < self.args.min_mel_length:
                continue

            # so now, mel is sufficiently long, and alignment looks good.
            # let's write the mel path to metadata.
            root_dir = self.config.path.raw_path
            rel_path = os.path.relpath(path, start=root_dir)
            wav_path = rel_path.replace(".predict", "")
            temp_meta.append((wav_path, speaker))

        return temp_meta


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-c",
        "--config",
        type=str,
        required=True,
        help="path to config.yaml",
    )
    parser.add_argument(
        "-p",
        "--checkpoint_path", 
        type=str,
        required=True,
        help="path to checkpoint",
    )
    parser.add_argument(
        "-l", 
        "--min_mel_length", 
        type=int, 
        default=33,
        help="minimal length of mel spectrogram. (segment_length // hop_length + 1) expected.",
    )
    args = parser.parse_args()

    config = OmegaConf.load(args.config)
    preprocessor = Preprocessor(config)
    preprocessor.build_from_path()

    extractor = GtaExtractor(args, config)
    extractor.main()