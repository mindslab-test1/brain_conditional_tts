import os
import argparse
import torch
from pytorch_lightning import Trainer
from pytorch_lightning.callbacks import ModelCheckpoint
from pytorch_lightning.plugins.training_type.ddp import DDPPlugin
from omegaconf import OmegaConf

from model.fastspeech2 import FastSpeech2
from utils.logger import FastSpeech2Logger


def main(args, config):
    model = FastSpeech2(config, args.use_vocoder, args.warm_start)
    save_path = config.path.ckpt_path
    os.makedirs(save_path, exist_ok=True)
    checkpoint_callback = ModelCheckpoint(
        dirpath=save_path,
        filename=None, 
        monitor="validation_loss/total_loss",
        verbose=True,
        save_top_k=args.save_top_k,
        mode="min",
    )

    log_path = config.path.log_path
    os.makedirs(log_path, exist_ok=True)
    fs2_logger = FastSpeech2Logger(save_dir=log_path)
    
    state = torch.load(args.baseline_path, map_location=lambda storage, loc: storage)
    pretrained_dict = state["state_dict"]
    new_model_dict = model.state_dict()

    print("Load the following modules from the checkpoint:")
    for k, v in pretrained_dict.items():
        if k in new_model_dict and new_model_dict[k].size() == v.size():
            #print(k)
            new_model_dict[k] = v

    model.load_state_dict(new_model_dict)
    del state

    model.warm_start = args.warm_start

    trainer = Trainer(
        accelerator="ddp",
        logger=fs2_logger,
        callbacks=[checkpoint_callback],
        checkpoint_callback=True,
        weights_save_path=save_path,
        gpus=-1 if args.gpus is None else args.gpus,
        num_sanity_val_steps=1,
        resume_from_checkpoint=None,
        gradient_clip_val=config.optimizer.grad_clip_thresh,
        fast_dev_run=args.fast_dev_run,
        check_val_every_n_epoch=config.step.val_step,
        progress_bar_refresh_rate=1,
        max_steps=config.step.total_step,
        plugins=[DDPPlugin(find_unused_parameters=True)],
    )
    trainer.fit(model)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-g", 
        "--gpus", 
        type=str, 
        default=None,
        help="Number of gpus to use (e.g. '0,1'). Will use all if not given.",
    )
    parser.add_argument(
        "-c",
        "--config",
        type=str,
        required=True,
        help="path to config.yaml",
    )
    parser.add_argument(
        "-b",
        "--baseline_path", 
        type=str, 
        default=None,
        help="path to checkpoint for transfer",
    )
    parser.add_argument(
        "-f", 
        "--fast_dev_run", 
        type=bool, 
        default=False,
        help="fast run for debugging purpose",
    )
    parser.add_argument(
        "-s",
        "--save_top_k", 
        type=int, 
        default=3,
        help="save top k checkpoints, -1 for save all",
    )
    parser.add_argument(
        "--use_vocoder", 
        action="store_true", 
        help="use pretrained vocoder to synthesize wav",
    )
    parser.add_argument(
        "--warm_start", 
        action="store_true",
        help="training speaker embedding only"
    )
    args = parser.parse_args()

    # Read Config
    config = OmegaConf.load(args.config)
    main(args, config)