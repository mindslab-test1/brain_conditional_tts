FROM nvcr.io/nvidia/pytorch:20.09-py3
RUN python3 -m pip uninstall -y \
    tensorboard \
    tensorboard-plugin-dlprof &&  \
python3 -m pip --no-cache-dir install --upgrade \
    pytorch_lightning==1.2.9 \
    tensorboard==2.0.0 \
    omegaconf==2.0.6 \
    gpustat==0.6.0 \
    grpcio==1.13.0 \
    grpcio-tools==1.13.0 \
    protobuf==3.6.0 \
    librosa==0.8.0 \
    matplotlib==3.3.1 \
    scipy==1.5.2 \
    pyworld==0.2.10 \
    tgt==1.4.4 \
    scikit-learn==0.23.2 \
    inflect==4.1.0 \
    Unidecode==1.1.1 \
    pypinyin==0.39.0 \
    && \
apt update && \
apt install -y \
    tmux \
    htop \
    ncdu && \
apt clean && \
apt autoremove && \
rm -rf /var/lib/apt/lists/* /tmp/* && \
mkdir /root/brain_fastspeech2
COPY . /root/brain_fastspeech2
RUN cd /root/brain_fastspeech2/ && \
    python3 -m grpc.tools.protoc --proto_path=. --python_out=. --grpc_python_out=. tts.proto