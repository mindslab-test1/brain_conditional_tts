import argparse
from omegaconf import OmegaConf

from preprocessor.preprocessor import Preprocessor


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
		"-c",
        "--config", 
        type=str, 
        help="path to preprocess.yaml",
    )
    parser.add_argument(
		"-s",
        "--separate", 
        action="store_true",
        help="true if do not set val dataset in config and choose randomly",
    )
    args = parser.parse_args()

    config = OmegaConf.load(args.config)
    preprocessor = Preprocessor(config)
    preprocessor.build_from_path(args.separate)