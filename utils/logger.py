from pytorch_lightning.loggers import TensorBoardLogger
from pytorch_lightning.utilities import rank_zero_only


class FastSpeech2Logger(TensorBoardLogger):
    def __init__(self, save_dir, name='', version=None, **kwargs):
        super().__init__(save_dir, name, version, **kwargs)

    @rank_zero_only
    def log_loss(self, mode, losses, step):
        losses = [l.item() for l in losses]
        self.experiment.add_scalar(mode + "_Loss/total_loss", losses[0], step)
        self.experiment.add_scalar(mode + "_Loss/mel_loss", losses[1], step)
        self.experiment.add_scalar(mode + "_Loss/mel_postnet_loss", losses[2], step)
        self.experiment.add_scalar(mode + "_Loss/pitch_loss", losses[3], step)
        self.experiment.add_scalar(mode + "_Loss/energy_loss", losses[4], step)
        self.experiment.add_scalar(mode + "_Loss/duration_loss", losses[5], step)
        
    @rank_zero_only
    def log_spectrogram(self, mode, fig, step):
        self.experiment.add_figure(mode, fig, step)
    
    @rank_zero_only
    def log_wav(self, mode, audio, step, sample_rate=22050):
        self.experiment.add_audio(
            mode, 
            audio / max(abs(audio)), 
            step, 
            sample_rate=sample_rate,
        )