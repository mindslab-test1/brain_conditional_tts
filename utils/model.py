import os
import json
import torch

import hifigan


def get_vocoder(config, device):
    name = config.vocoder.model
    ckpt_path = config.vocoder.path
    config_dir = os.path.split(ckpt_path)[0]

    if name == "HiFi-GAN":
        with open(os.path.join(config_dir, "config.json"), "r") as f:
            config = json.load(f)
        config = hifigan.AttrDict(config)
        vocoder = hifigan.Generator(config)
        ckpt = torch.load(ckpt_path)
        vocoder.load_state_dict(ckpt["model_g"])
        vocoder.eval()
        vocoder.remove_weight_norm()
        if device is not None:
            vocoder.to(device)

    return vocoder

def vocoder_infer(mels, vocoder, config, lengths=None):
    name = config.vocoder.model
    with torch.no_grad():
        if name == "HiFi-GAN":
            wavs = vocoder(mels).squeeze(1)

    wavs = (
        wavs.cpu().numpy() * config.preprocessing.audio.max_wav_value
    ).astype("int16")
    wavs = [wav for wav in wavs]

    for i in range(len(mels)):
        if lengths is not None:
            wavs[i] = wavs[i][: lengths[i]]

    return wavs