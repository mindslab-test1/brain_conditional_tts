import argparse
from omegaconf import OmegaConf

from preprocessor.preprocessor_mfa import MFA_Dictionary


def main(config, move=True):
	instance = MFA_Dictionary(config)
	if move:
		instance.prepare_align()
	instance.create_dictionary()


if __name__ == "__main__":
	parser = argparse.ArgumentParser()
	parser.add_argument(
		"-c",
		"--config", 
		type=str, 
		help="path to configuration yaml file",
	)
	parser.add_argument(
		"--not_move", 
		action="store_false",
		help="do not move data (case that you need only phoneme dictionary)",
	)
	args = parser.parse_args()

	config = OmegaConf.load(args.config)
	main(config, args.not_move)