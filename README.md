# FastSpeech 2

Microsoft의 [**FastSpeech 2: Fast and High-Quality End-to-End Text to Speech**](https://arxiv.org/abs/2006.04558) 구현 코드입니다.    
FastSpeech 2 버전 중 F0를 pitch feature로 사용한 [version 1](https://arxiv.org/abs/2006.04558v1)을 기반으로 하였습니다.   
또한 [**Montreal forced aligner (MFA)**](https://montreal-forced-aligner.readthedocs.io/en/latest/)의 한국어 데이터 적용을 용이하게 만들기 위해 일부 수정한 [dictinary generator](https://github.com/Jackson-Kang/Korean-phoneme-dictionary-generator)를 포함하고 있습니다.   
학습 및 서버 띄우기 메뉴얼은 [**FastSpeech 2 매뉴얼**](https://pms.maum.ai/confluence/pages/viewpage.action?pageId=35911855)에서 확인 바랍니다.

# Start Learning

## Docker
MFA와 nvidia pytorch dockerfile 사이의 충돌로 인해 MFA 학습용 Dockerfile을 따로 만들었습니다.   
MFA training은 MFA 도커 컨테이너 내에서 진행해주세요.   
Configuration file은 양식에 맞춰 작성한 뒤 사용해주세요.   

## Generating Dictionary
```
python3 generate_dictionary.py -c CONFIG_PATH
```
MFA 학습이 요구하는 포맷으로 데이터 위치를 옮기고 필요한 phoneme dictionary를 생성합니다.   
만약 데이터가 이미 요구되는 형식에 맞춰 정리되어 있다면 --not_move를 붙이면 이동 없이 phoneme dictionary만 생성합니다.   
이미 MFA를 통해 추출한 TextGrid가 있고 포맷이 맞춰져 있다면 바로 preprocessing으로 넘어가면 됩니다.

## MFA training
```
mfa train RAW_DATA_PATH DICTIONARY_PATH TEXTGRID_DIRECTORY_PATH
```
포맷에 맞게 정리된 corpus와 phoneme dictionary를 가지고 MFA 학습을 진행합니다.   
TextGrid 파일은 지정된 디렉토리에 저장됩니다.

## Preprocessing
```
python3 preprocess.py -c CONFIG_PATH
```
TextGrid 파일을 사용하여 데이터의 ground-truth duration/pitch/energy/mel 등을 추출, 저장하고 metafile을 생성합니다.   
만약 따로 validation metadata를 쓰지 않고 config에서 지정한 숫자만큼의 set을 분리해내고 싶다면 --separate를 붙이면 됩니다.

## Training
```
python3 trainer.py -g '0,1' -c CONFIG_PATH
```
준비된 데이터로 모델을 훈련시킵니다.   
사전학습된 vocoder checkpoint를 가지고 학습 중간에 음성을 확인하고 싶다면 --use_vocoder를 붙이면 tensorboard에서 확인할 수 있습니다.   
이전에 학습하던 모델을 이어서 학습시키고 싶다면 -r 뒤에 이어서 학습할 모델 checkpoint path를 적어주세요.

## Trasfer
```
python3 transfer.py -g '0,1' -c CONFIG_PATH -b CHECKPOINT_PATH
```
사전학습된 모델을 새 데이터에 맞춰 전이 학습시킵니다.   
새 데이터를 준비하는 과정을 동일하며, speaker embedding만 먼저 학습시킬 경우 --warm_start를 붙이면 됩니다.   
마찬가지로 사전학습된 vocoder checkpoint를 가지고 학습 중 음성을 확인하려면 --use_vocoder를 붙이면 됩니다.

## Inference

서버 없이 inference를 하려면 dev 브랜치 확인

# Others

## TensorBoard
```
tensorboard --port=6006 --logdir output/log/dataset --bind_all
```
Loss 변화 및 모델에 의해 합성된 Mel-spectrogram와 waveform을 확인할 수 있습니다.   
꼭 --bind_all을 붙여야 접근이 가능합니다.

## GTA Mel extraction for HiFi-GAN learning
```
python3 gta_extractor.py -c config/dataset/config.yaml -p checkpoints/baseline.ckpt
```
Vocoder 학습에 사용할 ground-truth aligned Mel을 추출하여 저장합니다.

## Model converter
```
python3 model_converter.py -c config/dataset/config.yaml -i checkpoints/training_model.ckpt -o checkpoints/inference_model.ckpt
```
Inference에 쓰기 위한 checkpoint를 생성합니다.

# References
- [FastSpeech 2: Fast and High-Quality End-to-End Text to Speech](https://arxiv.org/abs/2006.04558), Y. Ren, *et al*.
- [TensorSpeech's FastSpeech 2 implementation](https://github.com/TensorSpeech/TensorflowTTS)
- [xcmyz's FastSpeech implementation](https://github.com/xcmyz/FastSpeech)
- [rishikksh20's FastSpeech 2 implementation](https://github.com/rishikksh20/FastSpeech2)
- [Handong Deep-learning Lab's FastSpeech 2 implementation](https://github.com/HGU-DLLAB/Korean-FastSpeech2-Pytorch)