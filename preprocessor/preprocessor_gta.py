import os
import librosa
import numpy as np
from tqdm import tqdm

import audio as Audio


class Preprocessor:
    def __init__(self, config):
        self.in_dir = config.path.raw_path
        self.out_dir = config.path.preprocessed_path
        self.sampling_rate = config.preprocessing.audio.sampling_rate
        self.hop_length = config.preprocessing.stft.hop_length

        self.STFT = Audio.stft.TacotronSTFT(
            config.preprocessing.stft.filter_length,
            config.preprocessing.stft.hop_length,
            config.preprocessing.stft.win_length,
            config.preprocessing.mel.n_mel_channels,
            config.preprocessing.audio.sampling_rate,
            config.preprocessing.mel.mel_fmin,
            config.preprocessing.mel.mel_fmax,
        )

    def build_from_path(self):
        os.makedirs((os.path.join(self.out_dir, "hifigan_mel")), exist_ok=True)

        print("Processing Mel Data ...")
        speakers = {}
        for i, speaker in enumerate(tqdm(os.listdir(self.in_dir))):
            speakers[speaker] = i
            speaker_dataset = os.listdir(os.path.join(self.in_dir, speaker))
            for wav_name in speaker_dataset:
                if ".wav" not in wav_name:
                    continue
                basename = os.path.splitext(wav_name)[0]
                if (basename + ".lab") not in speaker_dataset:
                    continue
                if speaker in basename:
                    tg_path = os.path.join(
                        self.out_dir, "TextGrid", speaker, "{}.TextGrid".format(basename)
                    )
                else:
                    tg_path = os.path.join(
                        self.out_dir, "TextGrid", speaker, "{}.TextGrid".format(speaker + "_" + basename)
                    )
                
                if os.path.exists(tg_path):
                    self.process_utterance(speaker, basename)
                else:
                    continue

        return None

    def process_utterance(self, speaker, basename):
        wav_path = os.path.join(self.in_dir, speaker, "{}.wav".format(basename))
        
        # Read and trim wav files
        wav, _ = librosa.load(wav_path)

        # Compute mel-scale spectrogram and energy
        mel_spectrogram, _ = Audio.tools.get_mel_from_wav(wav, self.STFT)
        
        # Save files
        mel_filename = "{}-hifigan_mel-{}.npy".format(speaker, basename)
        np.save(
            os.path.join(self.out_dir, "hifigan_mel", mel_filename),
            mel_spectrogram.T,
        )

        return None
