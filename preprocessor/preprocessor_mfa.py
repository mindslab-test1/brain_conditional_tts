import os
import re
import librosa
import numpy as np
from scipy.io import wavfile
from tqdm import tqdm
from glob import glob
from unicodedata import normalize

from text.cleaners import collapse_whitespace


def read_file(source_path):
	with open(source_path, mode="r", encoding="utf-8-sig") as f:
		content = f.readline().rstrip()
	return content

def create_phoneme_dictionary(source_path):
	phoneme_dict = {}
	for lab_file in tqdm(glob(os.path.join(source_path, "**", "*.lab"))):
		phone = collapse_whitespace(read_file(lab_file)).strip()
		phoneme_list = normalize("NFKD", phone).split(" ")
		word_list = [w for w in phone.split(" ") if w != ""]

		for idx, word in enumerate(word_list):
			if re.sub(r"[^\w]", "", word) == "":
				continue

			if word not in phoneme_dict.keys():
				phoneme_dict[word] = " ".join(phoneme_list[idx])

	return phoneme_dict

def write_dictionary(savepath, dictionary):
	"""
		input-dict format
			key: word of transcript delimited by <space> (e.g. 국물이)	
			value: phoneme of hangul-word decomposed into syllables  (e.g. ㄱㅜㅇㅁㅜㄹㅣ)
				=> i.e., input dictionary must define word-phoneme mapping
	"""
	with open(savepath, "w", encoding="utf-8") as f:
		for key in dictionary.keys():
			content = "{}\t{}\n".format(key, dictionary[key])
			f.write(content)


class MFA_Dictionary():
	def __init__(self, config):
		os.makedirs(config.path.raw_path, exist_ok=True)
		self.source_dataset_path = config.path.corpus_path
		self.train_meta_paths = config.path.train_meta_paths
		self.val_meta_paths = config.path.val_meta_paths
		self.metadata_savepaths = self.train_meta_paths + self.val_meta_paths
		self.dictionary_savepath = config.path.phoneme_dictionary_path.format(config.dataset)
		self.sampling_rate = config.preprocessing.audio.sampling_rate
		self.max_wav_value = config.preprocessing.audio.max_wav_value
		self.savepath = config.path.raw_path
		
	def prepare_align(self):
		total = len(self.metadata_savepaths)
		for i, metadata_savepath in enumerate(self.metadata_savepaths):
			print("\n[LOG] processing metafile {}/{}".format(i + 1, total))
			with open(metadata_savepath, encoding="utf-8") as f:
				for line in tqdm(f):
					wav_name, text, speaker = line.strip("\n").split("|")
					wav_path = os.path.join(self.source_dataset_path, wav_name)

					text = collapse_whitespace(text)
					p = re.compile(r"[^\w]")
					converted = "".join(p.findall(text))

					#'-' replaces special characters
					text_converted = re.sub(r"[^\w\s]", "-", text)

					#'- ' replaces whitespace
					text_converted = re.sub(r"[\s]", "- ", text_converted)
					

					if os.path.exists(wav_path):
						os.makedirs(
							os.path.join(self.savepath, speaker), 
							exist_ok=True,
						)
						wav, _ = librosa.load(wav_path, self.sampling_rate)
						wav = wav / max(abs(wav)) * self.max_wav_value
						wav_name = wav_name.strip("\n").split("/")[-1]
						if i >= len(self.train_meta_paths):
							wav_name = wav_name.replace('.wav', '_isval.wav')
						lab_name = wav_name.replace(".wav", ".lab")
						converted_name = lab_name.replace(".lab", ".txt")
						wavfile.write(
							os.path.join(self.savepath, speaker, wav_name),
							self.sampling_rate,
							wav.astype(np.int16),
						)
						with open(
							os.path.join(self.savepath, speaker, lab_name), 
							"w",
						) as f1:
							f1.write(text_converted)

						with open(
							os.path.join(self.savepath, speaker, converted_name), 
							"w",
						) as f2:
							f2.write(converted)
						
	def create_dictionary(self):
		print("\n[LOG] create phoneme dictionary...")
		phoneme_dict = create_phoneme_dictionary(self.savepath)	

		print("\n[LOG] write phoneme dictionary...")	
		
		write_dictionary(
			savepath=self.dictionary_savepath, 
			dictionary=phoneme_dict,
		)
		print("[LOG] done!")