import torch
import argparse
from omegaconf import OmegaConf

from model.fastspeech2 import FastSpeech2


def convert_model(args):
    config = OmegaConf.load(args.config)
    model = FastSpeech2(config, use_vocoder=False)
    checkpoint = torch.load(args.input_model_path, map_location="cpu")
    pretrained_dict = checkpoint["state_dict"]
    new_model_dict = model.state_dict()

    print("Load the following modules from the checkpoint")
    for k, v in pretrained_dict.items():
        if k in new_model_dict and new_model_dict[k].size() == v.size():
            new_model_dict[k] = v

    model.load_state_dict(new_model_dict)

    # save path, not configs
    torch.save({
        "config": config, 
        "state_dict": model.state_dict(),
    }, args.output_model_path)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="fastspeech 2 model converter")
    parser.add_argument(
        "-c",
        "--config",
        type=str,
        required=True,
        help="path to config.yaml",
    )
    parser.add_argument(
        "-i", 
        "--input_model_path", 
        required=True, 
        type=str,
        help="training model path",
    )
    parser.add_argument(
        "-o", 
        "--output_model_path", 
        required=True, 
        type=str,
        help="inference model path",
    )
    args = parser.parse_args()

    convert_model(args)