import os
from utils.tools import to_device
import grpc
import time
import re
import torch
import logging
import argparse
import threading
import numpy as np
from concurrent import futures
from omegaconf import OmegaConf

from text import text_to_sequence
from unicodedata import normalize
from model.fastspeech2 import FastSpeech2

import tts_pb2 
from tts_pb2_grpc import add_AcousticServicer_to_server, AcousticServicer

_ONE_DAY_IN_SECONDS = 60 * 60 * 24


class FastSpeech2Impl(AcousticServicer):
    def __init__(self, checkpoint_path, is_fp16, backup_path=""):
        super().__init__()
        self.lock = threading.Lock()
        self.running_set = set()
        self.waiting_queue = []
        self.backup_path = backup_path
        if backup_path != "":
            if os.path.exists(backup_path):
                logging.info("Load Config File: {}".format(backup_path))
                backup_config = OmegaConf.load(backup_path)
                checkpoint_path = backup_config.path
            else:
                self._save_config(checkpoint_path)
        self.model_status = tts_pb2.ModelStatus()
        self.is_fp16 = is_fp16
        try:
            self._load_checkpoint(checkpoint_path)
        except Exception as e:
            self.model_status.state = tts_pb2.MODEL_STATE_NONE
            self.model_status.model.Clear()
            logging.exception(e)

    @torch.no_grad()
    def Txt2Mel(self, in_text, context):
        try:
            outputs = self._inference(in_text, context)
            if outputs is None:
                return tts_pb2.MelSpectrogram()

            mel_outputs_postnet = outputs[1].transpose(1, 2).half()
            sample_length = mel_outputs_postnet.size(-1) * self.mel_config.hop_length
            mel_outputs_postnet = mel_outputs_postnet.reshape(-1).cpu().tolist()
            
            mel = tts_pb2.MelSpectrogram(
                data=mel_outputs_postnet,
                sample_length=sample_length,
            )
            return mel

        except Exception as e:
            logging.exception(e)
            context.set_code(grpc.StatusCode.UNKNOWN)
            context.set_details(str(e))

        finally:
            with self.lock:
                self.running_set.discard(threading.get_ident())

    @torch.no_grad()
    def StreamTxt2Mel(self, in_text, context):
        try:
            outputs = self._inference(in_text, context)
            if outputs is not None:
                mel_outputs_postnet = outputs[1].transpose(1, 2).half()
                mel_outputs_postnet = mel_outputs_postnet.reshape(-1).cpu().tolist()

                mel = tts_pb2.MelSpectrogram(data=mel_outputs_postnet)
                yield mel

        except Exception as e:
            logging.exception(e)
            context.set_code(grpc.StatusCode.UNKNOWN)
            context.set_details(str(e))

        finally:
            with self.lock:
                self.running_set.discard(threading.get_ident())

    def GetMelConfig(self, empty, context):
        if self.model_status.state == tts_pb2.MODEL_STATE_RUNNING:
            return self.mel_config
        else:
            context.set_code(grpc.StatusCode.UNAVAILABLE)
            context.set_details(tts_pb2.ModelState.Name(self.model_status.state))
            return tts_pb2.MelConfig()

    def SetModel(self, in_model, context, is_running=False):
        try:
            result = None
            waiting_key = None
            with self.lock:
                if 0 < len(self.waiting_queue):
                    waiting_key = threading.get_ident()
                    self.waiting_queue.append(waiting_key)
                else:
                    if in_model == self.model_status.model or in_model.path == "":
                        if is_running:
                            self.running_set.update([threading.get_ident()])
                        result = tts_pb2.SetModelResult(result=True)
                    elif in_model != self.model_status.model:
                        if 0 < len(self.running_set):
                            waiting_key = threading.get_ident()
                            self.waiting_queue.append(waiting_key)
                        else:
                            if is_running:
                                self.running_set.update([threading.get_ident()])
                            result = self._set_model(in_model)

            if waiting_key is not None:
                while True:
                    with self.lock:
                        if self.waiting_queue[0] == waiting_key:
                            if in_model == self.model_status.model or in_model.path == "":
                                if is_running:
                                    self.running_set.update([threading.get_ident()])
                                del self.waiting_queue[0]
                                result = tts_pb2.SetModelResult(result=True)
                                break
                            elif in_model != self.model_status.model:
                                if 0 == len(self.running_set):
                                    if is_running:
                                        self.running_set.update([threading.get_ident()])
                                    del self.waiting_queue[0]
                                    result = self._set_model(in_model)
                                    break
                    time.sleep(0.01)
            return result
        except Exception as e:
            logging.exception(e)
            context.set_code(grpc.StatusCode.UNKNOWN)
            context.set_details(str(e))

    def GetModel(self, empty, context):
        return self.model_status
    
    def _inference(self, in_text, context):
        logging.debug("_inference/in_text/%s", in_text)
        result = self.SetModel(in_text.model, context, True)
        if not result.result:
            context.set_code(grpc.StatusCode.INVALID_ARGUMENT)
            context.set_details(result.error)
            return None
        if self.model_status.state != tts_pb2.MODEL_STATE_RUNNING:
            context.set_code(grpc.StatusCode.UNAVAILABLE)
            context.set_details(tts_pb2.ModelState.Name(self.model_status.state))
            return None
        if self.max_speaker < in_text.speaker:
            context.set_code(grpc.StatusCode.INVALID_ARGUMENT)
            context.set_details("exceeded maxspeaker")
            return None
        
        text = in_text.text
        text = re.sub(re.compile(r"\s+"), " ", text)
        text = "{" + "/".join(list(normalize("NFKD", text))) + "}"
        text = np.array([text_to_sequence(
            text,
            self.hp.preprocessing.text.text_cleaners, 
            self.hp.preprocessing.text.language,
        )])
        text_len = np.array([len(text[0])])

        speaker = in_text.speaker
        speaker = np.array([speaker])

        batch = (speaker, text, text_len, max(text_len))
        batch = to_device(batch, self.model.device)
        return self.model.forward(
            *batch, 
            s_control=0, 
            p_control=1.0, 
            e_control=1.0, 
            d_control=1.0,
        )   #control

    def _set_model(self, in_model):
        logging.debug("_set_model/in_model/%s", in_model)
        try:
            if not os.path.exists(in_model.path):
                raise RuntimeError("Model {} does not exist.".format(in_model.path))
        except Exception as e:
            logging.exception(e)
            return tts_pb2.SetModelResult(result=False, error=str(e))

        try:
            self._load_checkpoint(in_model.path)
            self._save_config(in_model.path)
            return tts_pb2.SetModelResult(result=True)
        except Exception as e:
            logging.exception(e)
            self.model_status.state = tts_pb2.MODEL_STATE_NONE
            self.model_status.model.Clear()
            return tts_pb2.SetModelResult(result=False, error=str(e))

    def _load_checkpoint(self, checkpoint_path):
        self.model_status.state = tts_pb2.MODEL_STATE_LOADING

        if checkpoint_path != self.model_status.model.path:
            self.model_status.model.path = checkpoint_path
            checkpoint = torch.load(checkpoint_path, map_location='cuda:0')

            if not hasattr(self, "model") \
                    or self.hp.preprocessing.audio != checkpoint["config"].preprocessing.audio \
                    or self.hp.preprocessing.stft != checkpoint["config"].preprocessing.stft \
                    or self.hp.preprocessing.mel != checkpoint["config"].preprocessing.mel \
                    or self.hp.preprocessing.pitch != checkpoint["config"].preprocessing.pitch \
                    or self.hp.preprocessing.energy != checkpoint["config"].preprocessing.energy \
                    or self.hp.model.transformer != checkpoint["config"].transformer \
                    or self.hp.model.variance_predictor != checkpoint["config"].variance_predictor \
                    or self.hp.model.variance_embedding != checkpoint["config"].variance_embedding:
                self.hp = checkpoint["config"]
                self.model = FastSpeech2(self.hp, use_vocoder=False).cuda()
                self.model.eval()
                self.model.freeze()

                if self.is_fp16:
                    from apex import amp
                    self.model.encoder, _ = amp.initialize(self.model.encoder, [], opt_level="O3")
                    self.model.speaker_emb, _ = amp.initialize(self.model.speaker_emb, [], opt_level="O3")
                    self.model.variance_adaptor, _ = amp.initialize(self.model.variance_adaptor, [], opt_level="O3")
                    self.model.decoder, _ = amp.initialize(self.model.decoder, [], opt_level="O3")
                    self.model.mel_linear, _ = amp.initialize(self.model.mel_linear, [], opt_level="O3")
                    self.model.postnet, _ = amp.initialize(self.model.postnet, [], opt_level="O3")
            
            if self.hp.preprocessing.text != checkpoint["config"].preprocessing.text:
                self.model.config.preprocessing.text = checkpoint["config"].preprocessing.text
                from transformer import Constants
                from text.symbols import en_symbols, ko_symbols, zh_symbols
                lang = checkpoint["config"].preprocessing.text.language
                
                if lang == "ko":
                    n_src_vocab = len(ko_symbols) + 1
                elif lang == "en":
                    n_src_vocab = len(en_symbols) + 1
                elif lang == "zh":
                    n_src_vocab = len(zh_symbols) + 1

                self.model.encoder.src_word_emb = torch.nn.Embedding(
                    n_src_vocab, 
                    checkpoint["config"].transformer.encoder_hidden, 
                    padding_idx=Constants.PAD
                )
                self.model.encoder.src_word_emb.cuda()
                self.model.encoder.src_word_emb.eval()
                if self.is_fp16:
                    self.model.encoder.src_word_emb.weight.data = self.model.encoder.src_word_emb.weight.data.half()

            if self.hp.n_speakers != checkpoint["config"].n_speakers:
                self.model.config.n_speakers = checkpoint["config"].n_speakers
                self.model.speaker_emb = torch.nn.Embedding(
                    checkpoint["config"].n_speakers, 
                    checkpoint["config"].transformer.encoder_hidden,
                )
                self.model.speaker_emb.cuda()
                self.model.speaker_emb.eval()
                if self.is_fp16:
                    self.model.speaker_emb.weight.data = self.model.speaker_emb.weight.data.half()

            self.model.load_state_dict(checkpoint["state_dict"])

            self.mel_config = tts_pb2.MelConfig(
                filter_length=self.hp.preprocessing.stft.filter_length,
                hop_length=self.hp.preprocessing.stft.hop_length,
                win_length=self.hp.preprocessing.stft.win_length,
                n_mel_channels=self.hp.preprocessing.mel.n_mel_channels,
                sampling_rate=self.hp.preprocessing.audio.sampling_rate,
                mel_fmin=self.hp.preprocessing.mel.mel_fmin,
                mel_fmax=self.hp.preprocessing.mel.mel_fmax,
            )
            self.max_speaker = self.hp.n_speaker

        self.model_status.state = tts_pb2.MODEL_STATE_RUNNING

    def _save_config(self, checkpoint_path):
        if self.backup_path != "":
            config = OmegaConf.create({
                "path": checkpoint_path
            })
            OmegaConf.save(config, self.backup_path)
            logging.debug("Save Config File: {}".format(self.backup_path))


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="fastspeech 2 runner executor")
    parser.add_argument(
        "-m", 
        "--model",
        nargs="?",
        dest="model",
        required=True,
        help="Model Path.",
        type=str,
    )
    parser.add_argument(
        "-l", 
        "--log_level",
        nargs="?",
        dest="log_level",
        help="logger level",
        type=str,
        default="INFO",
    )
    parser.add_argument(
        "-p", 
        "--port",
        nargs="?",
        dest="port",
        help="grpc port",
        type=int,
        default=30003,
    )
    parser.add_argument(
        "-w", 
        "--max_workers",
        nargs="?",
        dest="max_workers",
        help="max workers",
        type=int,
        default=4,
    )
    parser.add_argument(
        "--is_fp16", 
        action="store_true",
        help="fp16 mode",
    )
    parser.add_argument(
        "-b", 
        "--backup_config", 
        type=str, 
        default="",
        help="yaml file for backup configuration",
    )
    args = parser.parse_args()

    logging.basicConfig(
        level=getattr(logging, args.log_level),
        format="[%(levelname)s|%(filename)s:%(lineno)s][%(asctime)s] >>> %(message)s",
    )

    fastspeech2= FastSpeech2Impl(args.model, args.is_fp16, args.backup_config)

    server = grpc.server(futures.ThreadPoolExecutor(max_workers=args.max_workers),)
    add_AcousticServicer_to_server(fastspeech2, server)
    server.add_insecure_port("[::]:{}".format(args.port))
    server.start()

    logging.info("fastspeech2 starting at 0.0.0.0:%d", args.port)

    try:
        while True:
            # Sleep forever, since `start` doesn't block
            time.sleep(_ONE_DAY_IN_SECONDS)
    except KeyboardInterrupt:
        server.stop(0)