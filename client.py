import grpc
import argparse
import google.protobuf.empty_pb2 as empty

from tts_pb2_grpc import AcousticStub
from tts_pb2 import InputText, Model


class AcousticClient(object):
    def __init__(self, remote="127.0.0.1:30003"):
        channel = grpc.insecure_channel(remote)
        self.stub = AcousticStub(channel)

    def txt2mel(self, speaker, text):
        return self.stub.Txt2Mel(InputText(speaker=speaker, text=text))

    def stream_txt2mel(self, speaker, text):
        return self.stub.StreamTxt2Mel(InputText(speaker=speaker, text=text))

    def get_mel_config(self):
        return self.stub.GetMelConfig(empty.Empty())

    def set_model(self, checkpoint_path):
        raise NotImplementedError

    def get_model(self):
        return self.stub.GetModel(empty.Empty())


if __name__ == "__main__":
    example_text = '내가 지금보다 나이도 어리고 마음도 여리던 시절 아버지가 충고를 하나 해주션는데, 그 충고를 나는 아직또 마음쏘그로 되새기곤 한다. "누구를 비판하고 시퍼질 땐 마리다, 세상 사라미 다 너처럼 조은 조꺼늘 타고난 건 아니라는 저믈 명심하도록 해라."'
    parser = argparse.ArgumentParser(description="fastspeech 2 TTS client")
    parser.add_argument(
        "-r", 
        "--remote", 
        type=str, 
        default="127.0.0.1:30003",
        help="grpc: ip:port",
    )
    parser.add_argument(
        "-t", 
        "--text", 
        type=str, 
        default=example_text,
        help="input text. must be pre-processed by g2p.",
    )
    parser.add_argument(
        "-s", 
        "--speaker", 
        type=int, 
        default=0,
        help="speaker number",
    )
    args = parser.parse_args()
    client = AcousticClient(args.remote)

    mel_config = client.get_mel_config()
    print(mel_config)

    mel = client.txt2mel(args.speaker, args.text)
    print(mel.data)

    for mel in client.stream_txt2mel(args.speaker, args.text):
        print(mel.data)

    model = client.get_model()
    print(model)