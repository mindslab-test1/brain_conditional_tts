import os
import json
import torch
import torch.nn as nn
import pytorch_lightning as pl
from torch.utils.data import DataLoader

from utils.model import get_vocoder
from utils.tools import to_device, get_mask_from_lengths, synth_one_sample, synth_samples
from model.loss import FastSpeech2Loss
from model.optimizer import ScheduledOptim
from dataset import Dataset
from transformer import Encoder, Decoder, PostNet
from .modules import VarianceAdaptor


class FastSpeech2(pl.LightningModule):
    """ FastSpeech2 """

    def __init__(self, config, use_vocoder=False, warm_start=False):
        super().__init__()
        self.config = config
        self.encoder = Encoder(config)
        self.variance_adaptor = VarianceAdaptor(config)
        self.decoder = Decoder(config)
        self.mel_linear = nn.Linear(
            config.transformer.decoder_hidden,
            config.preprocessing.mel.n_mel_channels,
        )
        self.postnet = PostNet()
        self.speaker_emb = None
        if config.multi_speaker:
            if config.n_speaker:
                n_speaker = config.n_speaker
            else: 
                with open(
                    os.path.join(
                        config.path.preprocessed_path, "speakers.json"
                    ),
                    "r",
                ) as f:
                    n_speaker = len(json.load(f))
            self.speaker_emb = nn.Embedding(
                n_speaker,
                config.transformer.encoder_hidden,
            )
        self.warm_start = warm_start
        self.loss = FastSpeech2Loss(config)
        self.sampling_rate = config.preprocessing.audio.sampling_rate
        self.vocoder = get_vocoder(config, self.device) if use_vocoder else None
        self.logging = True

    def forward(
        self,
        speakers,
        texts,
        src_lens,
        max_src_len,
        mels=None,
        mel_lens=None,
        max_mel_len=None,
        p_targets=None,
        e_targets=None,
        d_targets=None,
        s_control=0,
        p_control=1.0,
        e_control=1.0,
        d_control=1.0,
    ):
        src_masks = get_mask_from_lengths(src_lens, max_src_len)
        mel_masks = (
            get_mask_from_lengths(mel_lens, max_mel_len)
            if mel_lens is not None
            else None
        )

        output = self.encoder(texts, src_masks)

        if self.speaker_emb is not None:
            spk_emb = self.speaker_emb(speakers).unsqueeze(1).expand(
                -1, max_src_len, -1
            )
            if output.dtype == torch.float16:
                spk_emb = spk_emb.half()
            output = output + spk_emb

        (
            output,
            p_predictions,
            e_predictions,
            log_d_predictions,
            d_rounded,
            mel_lens,
            mel_masks,
        ) = self.variance_adaptor(
            output,
            src_masks,
            mel_masks,
            max_mel_len,
            p_targets,
            e_targets,
            d_targets,
            s_control,
            p_control,
            e_control,
            d_control,
        )

        output, mel_masks = self.decoder(output, mel_masks)
        output = self.mel_linear(output)
        postnet_output = self.postnet(output) + output
        
        return (
            output,
            postnet_output,
            p_predictions,
            e_predictions,
            log_d_predictions,
            d_rounded,
            src_masks,
            mel_masks,
            src_lens,
            mel_lens,
        )

    def synthesize(self, batch, control_values):
        batch = to_device(batch, self.device)
        output = self.forward(
            *(batch[2:]),
            s_control=control_values[0],
            p_control=control_values[1],
            e_control=control_values[2],
            d_control=control_values[3],
        )
        synth_samples(
            batch,
            output,
            self.vocoder,
            self.config,
        )

    def training_step(self, batch, batch_idx):
        batch = to_device(batch, self.device)
        output = self.forward(*(batch[3:]))
        losses = self.loss(batch, output)
        values = {
            "training_loss/total_loss": losses[0],
            "training_loss/mel_loss": losses[1],
            "training_loss/mel_postnet_loss": losses[2],
            "training_loss/pitch_loss": losses[3],
            "training_loss/energy_loss": losses[4],
            "training_loss/duration_loss": losses[5],
        }
        self.log_dict(values, on_step=True)

        return {"loss": losses[0]}
 
    def validation_step(self, batch, batch_idx):
        batch = to_device(batch, self.device)
        output = self.forward(*(batch[3:]))
        losses = self.loss(batch, output)
        values = {
            "validation_loss/total_loss": losses[0],
            "validation_loss/mel_loss": losses[1],
            "validation_loss/mel_postnet_loss": losses[2],
            "validation_loss/pitch_loss": losses[3],
            "validation_loss/energy_loss": losses[4],
            "validation_loss/duration_loss": losses[5],
        }
        self.log_dict(values, on_epoch=True)

        if self.logging:
            fig, wav_recon, wav_pred, _ = synth_one_sample(
                batch,
                output,
                self.vocoder,
                self.config,
            )
            self.logger.log_spectrogram('Spectrogram', fig, self.global_step)
            if wav_recon is not None:
                self.logger.log_wav(
                    'Reconstructed', 
                    wav_recon, 
                    self.global_step,
                    self.sampling_rate,
                )
            if wav_pred is not None:
                self.logger.log_wav(
                    'Synthesized', 
                    wav_pred, 
                    self.global_step,
                    self.sampling_rate
                )
            self.logging = False
        
        return losses

    def validation_epoch_end(self, outputs):
        self.logging = True

    def configure_optimizers(self):
        if self.warm_start:
            self.optimizer = ScheduledOptim(
                self.speaker_emb, 
                self.config, 
                self.global_step
            )
        else:
            self.optimizer = ScheduledOptim(
                self, 
                self.config, 
                self.global_step
            )

        return self.optimizer._optimizer

    def optimizer_step(self, epoch, batch_idx, optimizer, optimizer_idx, optimizer_closure, on_tpu, using_native_amp, using_lbfgs):
        self.optimizer.step_and_update_lr(optimizer_closure)
        self.optimizer.zero_grad()

    def train_dataloader(self):
        train_dataset = Dataset(
            "train.txt", 
            self.config,
            train=True,
        )
        train_loader = DataLoader(
            train_dataset,
            batch_size=self.config.optimizer.train_batch_size,
            shuffle=True,
            collate_fn=train_dataset.collate_fn,
            num_workers=self.config.optimizer.num_worker,
        )
        
        return train_loader

    def val_dataloader(self):
        val_dataset = Dataset(
            "val.txt", 
            self.config,
            train=False,
        )
        val_loader = DataLoader(
            val_dataset,
            batch_size=self.config.optimizer.val_batch_size,
            shuffle=False,
            collate_fn=val_dataset.collate_fn,
            num_workers=self.config.optimizer.num_worker,
        )

        return val_loader